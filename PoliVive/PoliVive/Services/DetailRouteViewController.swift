//
//  DetailRouteViewController.swift
//  PoliVive
//
//  Created by Jose Leon on 7/16/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit
import Kingfisher
import Firebase

class DetailRouteViewController: SharedViewController, UITableViewDataSource, UITableViewDelegate {
    
    var routeOption:RouteOption = RouteOption()
    var name:String=""
    var rutaRegistrado:String=""
    var registrado = false
    
    @IBAction func buttonUpdate(_ sender: Any) {
        let currentUser = (Auth.auth().currentUser?.uid ?? nil)!
        let userRef = db.collection("users").document(currentUser)
        userRef.getDocument { (document, error) in
            if let document = document {
                let data = document.get("ruta") as! String
                if(data==""){
                    userRef.updateData(["ruta":self.name])
                    self.navigationController?.popViewController(animated: true)
                }else if(data==self.routeOption.route){
                    userRef.updateData(["ruta":""])
                    self.navigationController?.popViewController(animated: true)
                }else{
                    let alert = UIAlertController(title: "Error al registrar ruta", message: "Solo puede estar registrado en una sola ruta. ", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
                    self.present(alert, animated: true)
                }
            } else {
                print("Document does not exist")
            }
        }
    }
    
    @IBOutlet weak var typeTable: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 534;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        name=routeOption.route ?? ""
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "detailRouteCell") as! DetailRouteViewControllerCell
        cell.numeroDeRutaLabel.text=routeOption.route
        cell.descripcionRecorridoLabel.numberOfLines=0
        cell.descripcionRecorridoLabel.text=routeOption.travel
        cell.routeImageView.kf.setImage(with: URL(string: routeOption.routePicture ?? ""))
        if(name==rutaRegistrado){
            cell.registerButton.setTitle("Cancelar", for: .normal)
        }
        return cell
    }
    
    private let db = Firestore.firestore()
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var periodLabel: UILabel!
    
    func obtainInformation(){
        let currentUser = (Auth.auth().currentUser?.uid ?? nil)!
        let userRef = db.collection("users").document(currentUser)
        userRef.getDocument { (document, error) in
            if let document = document {
                let data = document.get("datos") as! [String]
                self.nameLabel.text?.append("\(data[0])")
                self.numberLabel.text?.append("\(data[1])")
                self.periodLabel.text?.append("\(data[2])")
                self.rutaRegistrado=document.get("ruta") as! String
            } else {
                print("Document does not exist")
            }
            self.typeTable.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        obtainInformation()
    }
}
