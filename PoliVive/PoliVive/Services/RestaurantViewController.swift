//
//  RestaurantViewController.swiftß
//  PoliVive
//
//  Created by Jose Leon on 7/16/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit
import Firebase
import Kingfisher

class RestaurantViewController: SharedViewController, UITableViewDataSource, UITableViewDelegate {
    
    var days = [DayOptions]()
    
    @IBOutlet weak var typeTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getDish()
        
    }
    
    func getDish() {
        let db = Firestore.firestore()
        db.collection("restaurante").order(by: "id").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    print("\(document.documentID) => \(document.data())")
                    let dayTemp:DayOptions = DayOptions()
                    dayTemp.nameDay = document.documentID
                    dayTemp.delDia=document.data()["DelDia"] as? String
                    dayTemp.especial=document.data()["Especial"] as? String
                    dayTemp.sopa1=document.data()["Sopa1"] as? String
                    dayTemp.sopa2=document.data()["Sopa2"] as? String
                    dayTemp.jugo=document.data()["Juga"] as? String
                    dayTemp.postre=document.data()["Postre"] as? String
                    dayTemp.postre2=document.data()["Postre2"] as? String
                    dayTemp.urlDelDia=document.data()["UrlDelDia"] as? String
                    dayTemp.urlEspecial=document.data()["UrlEspecial"] as? String
                    dayTemp.ensalada1=document.data()["ensalada1"] as? String
                    dayTemp.ensalada2=document.data()["ensalada2"] as? String
                    dayTemp.urlIcon=document.data()["icon"] as? String
                    self.days.append(dayTemp)
                }
                print(self.days)
                self.typeTable.reloadData()
                print("Datos cargados \(self.days.count)")
            }
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135.0;//Choose your custom row height
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.days.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "dia") as! RestaurantTableViewCell
        let currentDay = days[indexPath.row]
        cell.dayLabel.text = currentDay.nameDay
        cell.delDiaLabel.text = currentDay.delDia
        cell.especialLabel.text = currentDay.especial
        cell.dayIcon.kf.setImage(with: URL(string: currentDay.urlIcon ?? ""))
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetailRestaurant" {
            let destination = segue.destination as! DetailRestaurantViewController
            
            destination.dayOption = days[typeTable.indexPathForSelectedRow?.row ?? 0]
        }
    }
}


