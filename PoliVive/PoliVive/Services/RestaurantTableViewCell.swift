
import UIKit

class RestaurantTableViewCell: UITableViewCell {

    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var delDiaLabel: UILabel!
    @IBOutlet weak var especialLabel: UILabel!
    @IBOutlet weak var dayIcon: UIImageView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}
