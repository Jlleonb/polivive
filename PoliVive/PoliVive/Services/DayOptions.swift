//
//  DayOptions.swift
//  PoliVive
//
//  Created by Diego Portero on 7/19/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import Foundation

class DayOptions{
    var nameDay:String?
    var delDia:String?
    var especial:String?
    var sopa1:String?
    var sopa2:String?
    var jugo:String?
    var postre:String?
    var postre2:String?
    var precioDelDia:Float?
    var precioEspecial:Float?
    var ensalada1:String?
    var ensalada2:String?
    var urlDelDia:String?
    var urlEspecial:String?
    var urlIcon:String?
}
