//
//  DetailRouteViewControllerCell.swift
//  PoliVive
//
//  Created by Diego Portero on 7/26/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit

class DetailRouteViewControllerCell: UITableViewCell {
    @IBOutlet weak var nombreRutaLabel: UILabel!
    @IBOutlet weak var numeroDeRutaLabel: UILabel!
    @IBOutlet weak var descripcionRecorridoLabel: UILabel!
    @IBOutlet weak var routeImageView: UIImageView!
    @IBOutlet weak var registerButton: UIButton!
}
