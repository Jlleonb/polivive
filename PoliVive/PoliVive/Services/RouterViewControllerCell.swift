//
//  RouterViewControllerCell.swift
//  PoliVive
//
//  Created by Diego Portero on 7/26/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit

class RouterViewControllerCell: UITableViewCell {
    
    
    @IBOutlet weak var exitRoute: UILabel!
    @IBOutlet weak var comeRoute: UILabel!
    @IBOutlet weak var numRoute: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

}
