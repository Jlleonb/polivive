//
//  MapViewController.swift
//  PoliVive
//
//  Created by Jose Leon on 7/16/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: SharedViewController, CLLocationManagerDelegate,MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    let locationManager = CLLocationManager()
    let annotation = MKPointAnnotation()
    let annotationESFOT = MKPointAnnotation()
    let annotationQuimocaAgroindustria = MKPointAnnotation()
    let annotationCEC = MKPointAnnotation()
    let annotationTeatro = MKPointAnnotation()
    let annotationCivilAmbiental = MKPointAnnotation()
    let annotationMecaniaca = MKPointAnnotation()
    let annotationICB = MKPointAnnotation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        annotation.coordinate = CLLocationCoordinate2D(latitude: -0.21036983415821453, longitude: -78.48902257650852)
        annotation.title = "Facultad de Ingenieria en Sismemas"
        
        annotationESFOT.coordinate = CLLocationCoordinate2D(latitude: -0.21097332704925975, longitude: -78.48820718505223)
        annotationESFOT.title = "ESFOT"
        
        annotationQuimocaAgroindustria.coordinate = CLLocationCoordinate2D(latitude: -0.20989536060361047, longitude: -78.48918964335289)
        annotationQuimocaAgroindustria.title = "Facultad de Ingeniería Química y Agroindustrial"
        
        annotationCEC.coordinate = CLLocationCoordinate2D(latitude: -0.20915436861884018, longitude: -78.48689933215124)
        annotationCEC.title = "Edificio de Aulas y Relación con el Medio Externo- EARME"
        
        annotationTeatro.coordinate = CLLocationCoordinate2D(latitude: -0.2114863651097778, longitude: -78.49030316048749)
        annotationTeatro.title = "Teatro Politecnico"
        
        annotationCivilAmbiental.coordinate = CLLocationCoordinate2D(latitude: -0.2114863651097778, longitude: -78.49030316048749)
        annotationCivilAmbiental.title = "Facultad de Ingeniería Civil y Ambiental"
        
        annotationMecaniaca.coordinate = CLLocationCoordinate2D(latitude: -0.20930790428899115, longitude: -78.48947286504011)
        annotationMecaniaca.title = "Facultad de Ingeniería Mecánica"
        
        annotationICB.coordinate = CLLocationCoordinate2D(latitude: -0.20999990957652415, longitude: -78.49004953997604)
        annotationICB.title = "ICB"
        
        mapView.addAnnotation(annotation)
        mapView.addAnnotation(annotationESFOT)
        mapView.addAnnotation(annotationCEC)
        mapView.addAnnotation(annotationCivilAmbiental)
        mapView.addAnnotation(annotationTeatro)
        mapView.addAnnotation(annotationQuimocaAgroindustria)
        mapView.addAnnotation(annotationMecaniaca)
        mapView.addAnnotation(annotationICB)
        
        let userCoordinate = CLLocationCoordinate2D(latitude: -0.21036983415821453, longitude: -78.48902257650852)
        let eyeCoordinate = CLLocationCoordinate2D(latitude: -0.21036983415821453, longitude: -78.48902257650852)
        let mapCamera = MKMapCamera(lookingAtCenter: eyeCoordinate, fromEyeCoordinate: eyeCoordinate, eyeAltitude: 1600.0)
        mapView.setCamera(mapCamera, animated: true)
        
    }
}
