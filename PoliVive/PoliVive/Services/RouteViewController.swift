//
//  RouteViewController.swift
//  PoliVive
//
//  Created by Jose Leon on 7/16/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit
import Firebase


class RouteViewController: SharedViewController, UITableViewDataSource, UITableViewDelegate {
    
    var routes=[RouteOption]()
    var select:String=""
    var cellRegistrada:Int=(-1)
    
    @IBOutlet weak var typeTable: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.routes.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "routeCell") as! RouterViewControllerCell
        let currentRoute = routes[indexPath.row]
        cell.numRoute.text="Ruta \(currentRoute.numRoute ?? "")"
        cell.exitRoute.text="EPN"
        cell.comeRoute.text=currentRoute.route
        if(currentRoute.route==self.select){
            cell.backgroundColor = .blue
            cellRegistrada=indexPath.row
        }else{
            cell.backgroundColor = UIColor(red: 0.05, green: 0.04, blue: 0.29, alpha: 1.0)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 158;//Choose your custom row height
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let dba = Firestore.firestore()
        let currentUser = (Auth.auth().currentUser?.uid ?? nil)!
        let userRef = dba.collection("users").document(currentUser)
        userRef.getDocument { (document, error) in
            if let document = document {
                self.select = document.get("ruta") as! String
                self.getRoute()
            } else {
                print("Document does not exist")
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let dba = Firestore.firestore()
        let currentUser = (Auth.auth().currentUser?.uid ?? nil)!
        let userRef = dba.collection("users").document(currentUser)
        userRef.getDocument { (document, error) in
            if let document = document {
                self.select = document.get("ruta") as! String
                self.getRoute()
            } else {
                print("Document does not exist")
            }
            self.typeTable.reloadData()
        }
        
    }
    
    func getRoute(){
        let db = Firestore.firestore()
        if(self.routes.isEmpty){
            db.collection("route").order(by: "numRoute").getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    for document in querySnapshot!.documents {
                        let routeTemp:RouteOption = RouteOption()
                        routeTemp.numRoute=document.data()["numRoute"] as? String
                        routeTemp.route=document.data()["Route"] as? String
                        routeTemp.travel=document.data()["travel"] as? String
                        routeTemp.routePicture=document.data()["urlImage"] as? String
                        self.routes.append(routeTemp)
                    }
                    self.typeTable.reloadData()
                    print("Datos cargados \(self.routes.count)")
                }
            }
        }

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetailRoute" {
            let destination = segue.destination as! DetailRouteViewController
            destination.routeOption = routes[typeTable.indexPathForSelectedRow?.row ?? 0]
            if(typeTable.indexPathForSelectedRow?.row==cellRegistrada){
                destination.registrado=true
            }
        }
    }
}
