//
//  RouteOption.swift
//  PoliVive
//
//  Created by Diego Portero on 7/25/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import Foundation

class RouteOption {
    var numRoute:String?
    var route:String?
    var travel:String?
    var routePicture:String?
}
