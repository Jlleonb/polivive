//
//  DetailRestaurantViewControllerCell.swift
//  PoliVive
//
//  Created by Diego Portero on 7/19/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit

class DetailRestaurantViewControllerCell: UITableViewCell {

    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var sopa1Label: UILabel!
    @IBOutlet weak var sopa2Label: UILabel!
    @IBOutlet weak var platoFuerteLabel: UILabel!
    @IBOutlet weak var ensalada1Label: UILabel!
    @IBOutlet weak var ensalada2Label: UILabel!
    @IBOutlet weak var postre1Label: UILabel!
    @IBOutlet weak var postre2Label: UILabel!
    @IBOutlet weak var dishImageView: UIImageView!
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
