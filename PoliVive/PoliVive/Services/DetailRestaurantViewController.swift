//
//  DetailRestaurantViewController.swift
//  PoliVive
//
//  Created by Jose Leon on 7/16/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit
import Kingfisher

class DetailRestaurantViewController: SharedViewController, UITableViewDataSource, UITableViewDelegate {
    
    var dayOption:DayOptions = DayOptions()
    @IBOutlet weak var typeTable: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 550.0;
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "thisDish") as! DetailRestaurantViewControllerCell
        cell.sopa1Label.text = dayOption.sopa1
        cell.sopa2Label.text = dayOption.sopa2
        cell.postre1Label.text = dayOption.postre
        cell.ensalada1Label.text = dayOption.ensalada1
        cell.ensalada2Label.text = dayOption.ensalada2
        cell.postre2Label.text = dayOption.postre2
        print(indexPath)
        if indexPath.elementsEqual([0,0]) {
            cell.platoFuerteLabel.text = dayOption.delDia
            cell.dishImageView.kf.setImage(with: URL(string: dayOption.urlDelDia ?? ""))
            print(dayOption.urlDelDia)
        }else{
            cell.platoFuerteLabel.text = dayOption.especial
            cell.dishImageView.kf.setImage(with: URL(string: dayOption.urlEspecial ?? ""))
            cell.titleLabel.text = "Menu especial"
            print(dayOption.urlEspecial)
        }
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(dayOption.delDia)
    }
}
