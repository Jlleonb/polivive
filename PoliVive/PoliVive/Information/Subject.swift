//
//  Subject.swift
//  PoliVive
//
//  Created by Jose Leon on 7/25/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import Foundation

class Subject {
    var information:[String] = []
    
    init(_ anio:String,_ nombre:String,_ calificacion:String,_ creditos:String,_ estado:String) {
        self.information.append(nombre)
        self.information.append(anio)
        self.information.append(calificacion)
        self.information.append(creditos)
        self.information.append(estado)
    }
}
