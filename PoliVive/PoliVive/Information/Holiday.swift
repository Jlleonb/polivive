//
//  Holiday.swift
//  PoliVive
//
//  Created by Jose Leon on 7/19/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import Foundation

class Holiday {
    var description:String
    var date:String
    
    init(_ description:String, _ date:String) {
        self.description = description
        self.date = date
    }
}
