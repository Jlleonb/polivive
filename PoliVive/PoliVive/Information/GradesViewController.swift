//
//  GradesViewController.swift
//  PoliVive
//
//  Created by Jose Leon on 7/16/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit
import SpreadsheetView
import Firebase

class GradesViewController: SharedViewController, SpreadsheetViewDataSource {

    @IBOutlet weak var gradesTable: SpreadsheetView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var periodLabel: UILabel!
    
    private let db = Firestore.firestore()
    var grades : [Grades] = []
    private let titles : [String] = ["Materia", "Codigo", "1B", "2B", "3N", "final"]

    override func viewDidLoad() {
        super.viewDidLoad()
        gradesTable.gridStyle = .solid(width: 1.5, color: .black)
        gradesTable.dataSource = self
        gradesTable.register(DataCell.self, forCellWithReuseIdentifier: String(describing: DataCell.self))
        obtainInformation()
    }
    
    func obtainInformation(){
        let currentUser = (Auth.auth().currentUser?.uid ?? nil)!
        let userRef = db.collection("users").document(currentUser)
        userRef.getDocument { (document, error) in
            if let document = document {
                let data = document.get("datos") as! [String]
                self.nameLabel.text?.append("\(data[0])")
                self.numberLabel.text?.append("\(data[1])")
                self.periodLabel.text?.append("\(data[2])")
            } else {
                print("Document does not exist")
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getGrades(grades: { (grades) in
            self.grades = grades
            self.gradesTable.reloadData()
        })
    }
    
    func getGrades(grades:@escaping([Grades])->()){
        let currentUser = (Auth.auth().currentUser?.uid ?? nil)!
        db.collection("users").document(currentUser).collection("subjects").getDocuments {(snapshot, error) in
            var gradesInfo = [Grades]()
            if let error = error {
                print("Error getting documents: \(error)")
            } else {
                for document in snapshot!.documents {
                    let grade:Grades = Grades(document.documentID, document.get("codigo") as! String, document.get("firstB") as! String, document.get("secondB") as! String, document.get("thirdC") as! String, document.get("final") as! String)
                    gradesInfo.append(grade)
                }
                grades(gradesInfo)
            }
        }
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
        let cell = gradesTable.dequeueReusableCell(withReuseIdentifier: String(describing: DataCell.self), for: indexPath) as! DataCell
        cell.label.text = tableText(indexPath: indexPath)
        if (indexPath.row == 0){
            cell.label.font = UIFont.boldSystemFont(ofSize: 16.0)
        } 
        return cell
    }
    
    func tableText(indexPath: IndexPath) -> String {
        if indexPath.row == 0 {
            return titles[indexPath.column]
        } else {
            return grades[indexPath.row - 1].information[indexPath.column]
        }
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
        return 100
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
        return 75
    }
    
    func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
        return 1
    }
    
    func frozenColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return 1
    }
    
    func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return 6
    }
    
    func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
        return grades.count + 1
    }
}
