//
//  ScheduleViewController.swift
//  PoliVive
//
//  Created by Jose Leon on 7/16/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit
import Firebase
import SpreadsheetView

class ScheduleViewController: SharedViewController, SpreadsheetViewDataSource {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var periodLabel: UILabel!
    @IBOutlet weak var scheduleTable: SpreadsheetView!
    private let db = Firestore.firestore()
    private let titles : [String] = ["", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"]
    private let hours : [String] = ["", "07:00 - 09:00", "09:00 - 11:00", "11:00 - 13:00", "14:00 - 16:00", "16:00 - 18:00", "18:00 - 20:00"]
    private var schedule:[Schedule] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scheduleTable.gridStyle = .solid(width: 1.5, color: .black)
        scheduleTable.dataSource = self
        scheduleTable.register(DataCell.self, forCellWithReuseIdentifier: String(describing: DataCell.self))
        obtainInformation()
    }
    
    func obtainInformation(){
        let currentUser = (Auth.auth().currentUser?.uid ?? nil)!
        let userRef = db.collection("users").document(currentUser)
        userRef.getDocument { (document, error) in
            if let document = document {
                let data = document.get("datos") as! [String]
                self.nameLabel.text?.append("\(data[0])")
                self.numberLabel.text?.append("\(data[1])")
                self.periodLabel.text?.append("\(data[2])")
            } else {
                print("Document does not exist")
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getSchedule(scheduleRetrieve: { (scheduleRetrieve) in
            self.schedule = scheduleRetrieve
            self.scheduleTable.reloadData()
        })
    }
    
    func getSchedule(scheduleRetrieve:@escaping([Schedule])->()){
        let currentUser = (Auth.auth().currentUser?.uid ?? nil)!
        
        db.collection("users").document(currentUser).collection("schedule")
            .getDocuments{ (snapshot, error) in
                var scheduleInfo = [Schedule]()
                if let error = error {
                    print("Error getting documents: \(error)")
                } else {
                    for document in snapshot!.documents {
                        let schedule:Schedule = Schedule(document.get("07:00 - 09:00") as! String, document.get("09:00 - 11:00") as! String, document.get("11:00 - 13:00") as! String, document.get("14:00 - 16:00") as! String, document.get("16:00 - 18:00") as! String, document.get("18:00 - 20:00") as! String)
                        scheduleInfo.append(schedule)
                    }
                    scheduleRetrieve(scheduleInfo)
                }
                
        }
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
        let cell = scheduleTable.dequeueReusableCell(withReuseIdentifier: String(describing: DataCell.self), for: indexPath) as! DataCell
        cell.label.text = tableText(indexPath)
        if (indexPath.row == 0){
            cell.label.font = UIFont.boldSystemFont(ofSize: 16.0)
        }
        return cell
    }
    
    func tableText(_ indexPath:IndexPath) -> String{
        if (indexPath.row == 0){
            return self.titles[indexPath.column]
        } else {
            if (indexPath.column==0){
                return hours[indexPath.row]
            } else {
                return schedule[indexPath.column - 1 ].information[indexPath.row-1]
            }
        }
    }
    
    func frozenColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return 1
    }
    
    func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
        return 1
    }
    
    func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return 7
    }
    
    func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
        return schedule.count + 1
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
        return 110
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
        return 50
    }
}

