//
//  Grade.swift
//  PoliVive
//
//  Created by Jose Leon on 7/24/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import Foundation

class Grades {
    var information:[String] = []
    
    init(_ name:String,_ code:String,_ firstGrade:String,_ secondGrade:String,_ thirdGrade:String,_ final:String){
        self.information.append(name)
        self.information.append(code)
        self.information.append(firstGrade)
        self.information.append(secondGrade)
        self.information.append(thirdGrade)
        self.information.append(final)
    }
}
