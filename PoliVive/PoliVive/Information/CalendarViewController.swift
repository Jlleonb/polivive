//
//  CalendarViewController.swift
//  PoliVive
//
//  Created by Jose Leon on 7/16/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit
import SpreadsheetView
import Firebase

class CalendarViewController: SharedViewController, SpreadsheetViewDataSource {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var periodLabel: UILabel!
    @IBOutlet weak var calendarTable: SpreadsheetView!
    
    var calendar : [Holiday] = []
    private let db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        calendarTable.gridStyle = .solid(width: 1.5, color: .lightGray)
        calendarTable.dataSource = self
        calendarTable.register(DataCell.self, forCellWithReuseIdentifier: String(describing: DataCell.self))
        obtainInformation()
    }
    
    func obtainInformation(){
        let currentUser = (Auth.auth().currentUser?.uid ?? nil)!
        let userRef = db.collection("users").document(currentUser)
        userRef.getDocument { (document, error) in
            if let document = document {
                let data = document.get("datos") as! [String]
                self.nameLabel.text?.append("\(data[0])")
                self.numberLabel.text?.append("\(data[1])")
                self.periodLabel.text?.append("\(data[2])")
            } else {
                print("Document does not exist")
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        obtainDates(holidays: { (holidays) in
            self.calendar = holidays
            self.calendarTable.reloadData()
        })
    }
    

    func obtainDates(holidays:@escaping([Holiday])->()){
        db.collection("calendar").order(by: "id").getDocuments() { (snapshot, err) in
            var calendar = [Holiday]()
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in snapshot!.documents {
                    calendar.append(Holiday(document.get("description") as! String, document.get("date") as! String))
                }
                holidays(calendar)
            }
        }
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
        let cell = spreadsheetView.dequeueReusableCell(withReuseIdentifier: String(describing: DataCell.self), for: indexPath) as! DataCell
        cell.label.text = cellType(indexPath: indexPath)
        
        if (indexPath.row == 0){
            cell.label.font = UIFont.boldSystemFont(ofSize: 16.0)
            cell.gridlines.bottom = .solid(width: 3, color: .black)
            cell.gridlines.top = .solid(width: 3, color: .black)
            cell.gridlines.left = .solid(width: 3, color: .black)
            cell.gridlines.right = .solid(width: 3, color: .black)
        }
        return cell
        
    }
    
    func cellType(indexPath: IndexPath) -> String{
        if (indexPath.row == 0){
            
            if (indexPath.column == 0){
                return "Descripcion"
            } else if (indexPath.column == 1){
                return "Fecha"
            }
        } else {
            if (indexPath.column == 0){
                return calendar[indexPath.row - 1].description
            } else {
                return calendar[indexPath.row - 1].date
            }
        }
        return ""
    }
    
    func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return 2
    }
    
    func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
        return calendar.count + 1
    }
    
    func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
        return 1
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
        return 190
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
        return 50
    }
    
}
