//
//  CurriculumViewController.swift
//  PoliVive
//
//  Created by Jose Leon on 7/16/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit
import Firebase
import SpreadsheetView

class CurriculumViewController: SharedViewController, SpreadsheetViewDataSource {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var periodLabel: UILabel!
    @IBOutlet weak var curriculumTable: SpreadsheetView!
    
    private let db = Firestore.firestore()
    private var curriculum:[Subject] = []
    private let titles : [String] = [ "Nombre", "Año", "Calificación", "Créditos", "Estado"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        curriculumTable.gridStyle = .solid(width: 1.5, color: .black)
        curriculumTable.dataSource = self
        curriculumTable.register(DataCell.self, forCellWithReuseIdentifier: String(describing: DataCell.self))
        obtainInformation()
    }
    
    func obtainInformation(){
        let currentUser = (Auth.auth().currentUser?.uid ?? nil)!
        let userRef = db.collection("users").document(currentUser)
        userRef.getDocument { (document, error) in
            if let document = document {
                let data = document.get("datos") as! [String]
                self.nameLabel.text?.append("\(data[0])")
                self.numberLabel.text?.append("\(data[1])")
                self.periodLabel.text?.append("\(data[2])")
            } else {
                print("Document does not exist")
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getCurriculum(subjects: { (subjects) in
            self.curriculum = subjects
            self.curriculumTable.reloadData()
        })
    }
    
    func getCurriculum(subjects:@escaping([Subject])->()){
        let currentUser = (Auth.auth().currentUser?.uid ?? nil)!
        db.collection("users").document(currentUser).collection("curriculum").getDocuments{
            (snapshot, error) in
            var subjectsInfo = [Subject]()
            if let error = error {
                print("Error getting documents: \(error)")
            } else {
                for document in snapshot!.documents {
                    let subject:Subject = Subject(document.get("anio") as! String, document.documentID, document.get("calificacion") as! String, document.get("creditos") as! String, document.get("estado") as! String)
                    subjectsInfo.append(subject)
                }
                subjects(subjectsInfo)
            }
        }
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
        let cell = curriculumTable.dequeueReusableCell(withReuseIdentifier: String(describing: DataCell.self), for: indexPath) as! DataCell
        cell.label.text = tableText(indexPath: indexPath)
        if (indexPath.row == 0){
            cell.label.font = UIFont.boldSystemFont(ofSize: 16.0)
        }
        return cell
    }
    
    func tableText(indexPath:IndexPath) -> String{
        if (indexPath.row == 0){
            return self.titles[indexPath.column]
        } else {
            return curriculum[indexPath.row - 1 ].information[indexPath.column]
        }
    }
    
    func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return 5
    }
    
    func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
        return curriculum.count + 1
    }
    
    func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
        return 1
    }
    
    func frozenColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return 1
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
        return 110
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
        return 50
    }
    
}
