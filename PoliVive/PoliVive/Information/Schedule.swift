//
//  Schedule.swift
//  PoliVive
//
//  Created by Jose Leon on 8/1/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import Foundation

class Schedule {
    var information : [String] = []
    
    init(_ c:String, _ u:String, _ d:String, _ t:String, _ ct:String, _ cc:String) {
        self.information.append(c)
        self.information.append(u)
        self.information.append(d)
        self.information.append(t)
        self.information.append(ct)
        self.information.append(cc)
    }
}


