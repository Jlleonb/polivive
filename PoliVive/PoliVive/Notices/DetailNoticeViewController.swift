//
//  DetailNoticeViewController.swift
//  PoliVive
//
//  Created by Jose Leon on 7/16/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit

class DetailNoticeViewController: SharedViewController, UITableViewDataSource , UITableViewDelegate {
    
    @IBOutlet weak var tableViewNoticeDetail: UITableView!
    var newDetail = Notice()
 
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 896
    }
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView
            .dequeueReusableCell(withIdentifier: "newsTableDetailCell") as! NoticeDetailTableViewCell

        cell.textFieldNoticeDetailTitle.text = newDetail.title?.uppercased()
        cell.textFieldNoticeDetailDate.text = newDetail.date
        cell.labelNoticeDetailDescription.text = newDetail.descrption
        cell.imgNoticeDetail.kf.setImage(with: URL(string: newDetail.imageUrl ?? ""))
        
        return cell
    }
    

    
    
}
