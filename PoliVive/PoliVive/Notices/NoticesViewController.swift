//
//  NoticesViewController.swift
//  PoliVive
//
//  Created by Diego Portero on 7/2/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit
import Firebase
import Kingfisher

class NoticesViewController: SharedViewController, UITableViewDataSource , UITableViewDelegate {
    
    @IBOutlet weak var noticeTable: UITableView!
    
    var news = [Notice]()

    override func viewDidLoad() {
        super.viewDidLoad()
        getNews()
    }
    
    func getNews(){
        let db = Firestore.firestore()
        db.collection("news").getDocuments(){
            (querySnapshot,err) in
            if let err = err {
                print("Error getting news documents: \(err)")
            }else{
                for document in querySnapshot!.documents{
                    let newTemp: Notice = Notice()
                    newTemp.id = document.documentID
                    newTemp.title = document.data()["title"] as? String
                    newTemp.date = document.data()["date"] as? String
                    newTemp.descrption = document.data()["description"] as? String
                    newTemp.imageUrl = document.data()["imageUrl"] as? String
                    self.news.append(newTemp)
                    print(newTemp.title)
                }
                print(self.news)
                self.noticeTable.reloadData()
                print("Datos sin cargar")
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.news.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 373
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "newsTableCell") as! NoticeTableViewCell
        let currentNew = news[indexPath.row]
        print(currentNew.title)
        cell.noticeTitle.text = currentNew.title?.uppercased()
        cell.noticeDate.text = currentNew.date
        cell.noticeImage.kf.setImage(with: URL(string: currentNew.imageUrl ?? ""))
        
        
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetailNotice" {
            let destination = segue.destination as! DetailNoticeViewController
            destination.newDetail = news[noticeTable.indexPathForSelectedRow?.row ?? 0]
        }
    }
    

}
