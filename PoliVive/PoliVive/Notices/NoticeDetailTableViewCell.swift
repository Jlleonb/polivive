//
//  NoticeDetailTableViewCell.swift
//  PoliVive
//
//  Created by Johanna Cerezo on 8/1/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit

class NoticeDetailTableViewCell: UITableViewCell {

    
    @IBOutlet weak var imgNoticeDetail: UIImageView!
    @IBOutlet weak var textFieldNoticeDetailTitle: UILabel!
    @IBOutlet weak var textFieldNoticeDetailDate: UILabel!
    
    @IBOutlet weak var labelNoticeDetailDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
