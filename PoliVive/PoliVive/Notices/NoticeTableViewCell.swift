//
//  NoticeTableViewCell.swift
//  PoliVive
//
//  Created by Johanna Cerezo on 7/19/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit

class NoticeTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var noticeImage: UIImageView!
    @IBOutlet weak var noticeTitle: UILabel!
    @IBOutlet weak var noticeDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
