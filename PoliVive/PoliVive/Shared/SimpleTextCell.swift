//
//  SimpleTextCell.swift
//  PoliVive
//
//  Created by Jose Leon on 7/18/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import Foundation
import SpreadsheetView

class DataCell : Cell{
    let label = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        label.frame = bounds
        label.numberOfLines = 5
        label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.textAlignment = .center
        
        contentView.addSubview(label)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
