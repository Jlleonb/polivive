//
//  SharedViewController.swift
//  PoliVive
//
//  Created by Diego Portero on 7/2/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit
import Firebase

class SharedViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let button = UIBarButtonItem(title: "Sign out", style: .done, target: self, action: #selector(signOutTapped))
        self.navigationItem.rightBarButtonItem = button

    }
    
    @objc func signOutTapped(){
        try! Auth.auth().signOut()
        dismiss(animated: true, completion: nil)
    }

}
