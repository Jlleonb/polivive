//
//  DetailEventViewController.swift
//  PoliVive
//
//  Created by Jose Leon on 7/16/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit

class DetailEventViewController: SharedViewController, UITableViewDataSource , UITableViewDelegate {
    
    var eventDetail = Event()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 896
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventDetailTableCell") as! DetailEventTableViewCell
        cell.labelEventDetailTitle.text = eventDetail.title?.uppercased()
        cell.labelEventDetailDate.text = eventDetail.date
        cell.labelEventDetailPlace.text = eventDetail.place
        cell.labelEventDetailDescription.text = eventDetail.descrption
        cell.imgEventDetail.kf.setImage(with: URL(string: eventDetail.imageUrl ?? ""))
        
        
        return cell
    }
    
    
    
    
    
    
    
} 
