//
//  EventsViewController.swift
//  PoliVive
//
//  Created by Jose Leon on 7/16/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit
import Firebase
import Kingfisher

class EventsViewController: SharedViewController, UITableViewDataSource , UITableViewDelegate {
    
    @IBOutlet weak var eventsTableView: UITableView!
    var events = [Event]()
    
    
    override func viewDidLoad() {
        getEvents()
        super.viewDidLoad()
    }
    
    
    
    
    func getEvents(){
        let db = Firestore.firestore()
        db.collection("events").getDocuments(){
            (querySnapshot,err) in
            if let err = err {
                print("Error getting news documents: \(err)")
            }else{
                for document in querySnapshot!.documents{
                    let eventTemp: Event = Event()
                   eventTemp.id = document.documentID
                    eventTemp.title = document.data()["title"] as? String
                    eventTemp.date = document.data()["date"] as? String
                    eventTemp.place = document.data()["place"] as? String
                    eventTemp.descrption = document.data()["description"] as? String
                    eventTemp.imageUrl = document.data()["imgUrl"] as? String
                    self.events.append(eventTemp)
       
                }
                print(self.events)
                self.eventsTableView.reloadData()
                print("Datos sin cargar")
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.events.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 401
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventsTableCell") as! EventTableViewCell
        let currentEvent = events[indexPath.row]
        
        cell.labelEventTitle.text = currentEvent.title?.uppercased()
        cell.labelDateEvent.text = currentEvent.date
        cell.labelPlaceEvent.text = currentEvent.place
        cell.imgEvent.kf.setImage(with: URL(string: currentEvent.imageUrl ?? ""))
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toEventDetail" {
            let destination = segue.destination as! DetailEventViewController
            destination.eventDetail = events[eventsTableView.indexPathForSelectedRow?.row ?? 0]
        }
    }
}
