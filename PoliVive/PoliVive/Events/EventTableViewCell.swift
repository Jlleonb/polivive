//
//  EventTableViewCell.swift
//  PoliVive
//
//  Created by Johanna Cerezo on 8/1/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit

class EventTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var imgEvent: UIImageView!
    @IBOutlet weak var labelEventTitle: UILabel!
    @IBOutlet weak var labelDateEvent: UILabel!
    @IBOutlet weak var labelPlaceEvent: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
