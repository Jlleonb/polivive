//
//  DetailEventTableViewCell.swift
//  PoliVive
//
//  Created by Johanna Cerezo on 8/1/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit

class DetailEventTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var imgEventDetail: UIImageView!
    @IBOutlet weak var labelEventDetailTitle: UILabel!
    @IBOutlet weak var labelEventDetailDate: UILabel!
    @IBOutlet weak var labelEventDetailPlace: UILabel!
    
    @IBOutlet weak var labelEventDetailDescription: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
