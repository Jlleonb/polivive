//
//  LoginViewController.swift
//  PoliVive
//
//  Created by Diego Portero on 7/2/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        guard Auth.auth().currentUser != nil else {
            return
        }
        self.performSegue(withIdentifier: "toMainScreen", sender: self)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
    
    @IBAction func loginButton(_ sender: Any) {
        firebaseAuth(email: emailTextField.text!, password: passwordTextField.text!)
    }
    
    func firebaseAuth (email:String, password:String){
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if let _ = error {
                self.showAlertError()
                return
            }
            self.passwordTextField.text = ""
            self.performSegue(withIdentifier: "toMainScreen", sender: self)
        }
    }
    
    func showAlertError(){
        let alertView = UIAlertController(title: "Error", message: "Please enter valid credentials", preferredStyle: .alert)
        let okAlertAction = UIAlertAction(title:"OK",style: .default) { [unowned self] (_) in
            self.passwordTextField.text = ""
        }
        alertView.addAction(okAlertAction)
        present(alertView, animated: true, completion: nil)
    }
    
}
